﻿using System;
using System.Collections;

namespace Harsh_12Aug_Program
{
    public class Employee
    {
        int id;
        string name;
        public Employee(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public string getName()
        {
            return name;
        }

        public void setName(string name)
        {
            this.name = name;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList arrayList = new ArrayList();
            arrayList.Add(new Employee(21, "Harsh"));
            arrayList.Add(new Employee(22, "Vishal"));
            arrayList.Add(new Employee(23, "Yash"));
            arrayList.Add(new Employee(24, "Shantan"));
            arrayList.Add(new Employee(25, "Sharan"));

            foreach(Employee emp in arrayList)
            {
                Console.WriteLine(emp.getId() + " " + emp.getName());
            }


        }
    }
}
