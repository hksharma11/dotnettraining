﻿using System;

namespace Harsh_Program5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Simple Interest");
            int principal;
            int time;
            double rate;

            Console.WriteLine("Enter principal value");
            principal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter time ");
            time = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter rate");
            rate = Convert.ToDouble(Console.ReadLine());
            if(principal!=0 && rate!=0 && time!= 0)
            {
                double simpleInterest = (principal * rate * time) / 100;
                Console.WriteLine("The simple interest is " + simpleInterest);
            }
            else{
                Console.WriteLine("Invalid Input");
            }
            

        }
    }
}
