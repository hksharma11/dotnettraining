﻿using System;
using System.IO;
namespace Harsh_12Aug_InputOutput
{
    class Program
    {
        static void Main(string[] args)
        {
           FileStream fs = new FileStream(@"E:\sample.txt", FileMode.Open, FileAccess.Read);

            //StreamWriter sw = new StreamWriter(fs);
            StreamReader sr = new StreamReader(@"E:\sample.txt");
            sr.BaseStream.Seek(0, SeekOrigin.Begin);

            string str = sr.ReadLine();
            while(str!=null)
            {
                Console.WriteLine(str);
                str = sr.ReadLine();
            }
        }
    }
}
