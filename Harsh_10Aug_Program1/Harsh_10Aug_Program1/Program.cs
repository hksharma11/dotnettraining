﻿using System;

namespace Harsh_10Aug_Program1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number");
            int input = Convert.ToInt32(Console.ReadLine());
            int count = 0;
            for(int i=2;i<input;i++)
            {
                if(input%i==0)
                {
                    count++;
                }
            }
            if(count==0)
                Console.WriteLine("Prime");
            else
                Console.WriteLine("Not Prime");
            
        }
    }
}
