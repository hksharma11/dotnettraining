﻿using System;

namespace Harsh_12Aug_Generics
{
    public class SomeClass
    {
        public void GenericMethod<T,T2>(T Params1 , T2 Params2)
        {
            Console.WriteLine("First parameter is " + Params1 + " second parameter is " + Params2);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            SomeClass obj = new SomeClass();
            obj.GenericMethod<int,string>(1, "Harsh");
        }
    }
}
