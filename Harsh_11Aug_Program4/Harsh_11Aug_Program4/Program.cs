﻿using System;

namespace Harsh_11Aug_Program4
{
    abstract class Str
    {

        public abstract string makeUpper(string str);

    }

    class Text : Str
    {
        public override string makeUpper(string str)
        {
            return str.ToUpper();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Text text = new Text();
            Console.WriteLine(text.makeUpper("My name is Harsh Kumar Sharma"));

            Strength strength = new Strength();
            strength.pushUp();

            Cardio cardio = new Cardio();
            cardio.running();
        }
    }
}
