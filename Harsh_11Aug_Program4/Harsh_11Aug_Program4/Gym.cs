﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harsh_11Aug_Program4
{
   abstract class Gym
    {
        public abstract void running();
        public abstract void pushUp();
    }

    class Strength : Gym
    {
        public override void running()
        {
            throw new NotImplementedException();
        }

        public override void pushUp()
        {
            Console.WriteLine("Do 10 Push-ups");
        }
    }
    class Cardio : Gym
    {
        public override void running()
        {
            Console.WriteLine("Run 1200 meters");
        }

        public override void pushUp()
        {
            throw new NotImplementedException();
        }
    }
}
