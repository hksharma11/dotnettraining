﻿using System;

namespace Harsh_11Aug_Program1
{

    public class Automo
    {
        public int test = 100;
        public void fun()
        {
            Console.WriteLine("I am in automo");
        }
    }

    public class Repair:Automo
    {
        private void fun()
        {
            Console.WriteLine("I am in Repair");
        }
    }
    class Program:Repair
    {
        public void fun()
        {
            Console.WriteLine("I am in Program");
        }
        static void Main(string[] args)
        {
            Program obj = new Program();
            Console.WriteLine(obj.test);
            obj.fun();

            Automo newobj = new Program();
            newobj.fun();

            Automo autoobj = new Repair();
            autoobj.fun();

            Repair repobj = new Repair();
            repobj.fun();

            Cricket criketObject = new Cricket();
            criketObject.fun();

            Vehicle vehicle = new Vehicle();
            Console.WriteLine(vehicle.value);

        }
    }
}
