﻿using System;

namespace Harsh_10Aug_Program4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] marks = new int[6];
            int total = 0;
            for(int i=0;i<6;i++)
            {
                Console.WriteLine("Enter mark for subject " + (i+1));
                marks[i] = Convert.ToInt32(Console.ReadLine());
                total += marks[i];
            }
            Console.WriteLine("Percentage is " + (total *100)/600);


        }
    }
}
