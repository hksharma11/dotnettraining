﻿using System;

namespace Harsh_11Aug_Program3
{
    abstract class Shape
    {
        
        public abstract int Area(int side);

    }

    class Cube : Shape
    {
        public override int Area(int side)
        {
            return side * side * side;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Cube cube = new Cube();
            Console.WriteLine(cube.Area(3));
        }
    }
}
