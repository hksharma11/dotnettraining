﻿using System;

namespace Harsh_StringOperations_10Aug_12_10PM
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("String Operations");
            string firstName = "Harsh";
            string middleNmae = "kumar";
            string lastNmae = "Sharma";
           

            Console.WriteLine("Concate : " + string.Concat(firstName, middleNmae, lastNmae));
            Console.WriteLine("ToUpper : " + (firstName+" "+lastNmae).ToUpper());
            Console.WriteLine("ToLower : " + (firstName + " " + lastNmae).ToLower());
            Console.WriteLine("Replace : " + firstName.Replace('H','K'));
            Console.WriteLine("Length : " + (firstName+middleNmae+lastNmae).Length);
            Console.WriteLine("IndexOf : "+ firstName.IndexOf('a'));
            Console.WriteLine("Substring : " + firstName.Substring(2));
           
        }
    }
}
