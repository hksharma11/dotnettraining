﻿using System;

namespace Harsh_Program1_10_Aug_2_50
{
    class Program
    {
        public int sum(int input1,int  input2)
        {
            return input1 + input2;
        }
        static void Main(string[] args)
        {

            int num1, num2;
            Console.WriteLine("Enter first number");
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter second number");
            num2 = Convert.ToInt32(Console.ReadLine());
            Program obj = new Program();


            Console.WriteLine($"The sum of {num1} and {num2} is {obj.sum(num1, num2)}");

        }
    }
}
