﻿using System;
using System.Threading;

namespace Harsh_12Aug_Thread
{
    
    class Program
    {
        void work1()
        {
            Thread.Sleep(5000);
            Console.WriteLine("Hello World!");
        }

        void work2()
        {
            Thread.Sleep(7000);
            Console.WriteLine("New thread executed");
        }

        static void Main(string[] args)
        {
            Program obj = new Program();
            Thread thread = new Thread(obj.work1);
            Thread thread1 = new Thread(obj.work2);
            thread.Start();
            thread1.Start();

        }
    }
}
