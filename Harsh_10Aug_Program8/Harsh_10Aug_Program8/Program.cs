﻿using System;

namespace Harsh_10Aug_Program8
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "Harsh", "Sharan", "Vishal", "Sai", "Shanatan" };
            foreach(var name  in names)
            {
                Console.WriteLine(name); 
            }
        }
    }
}
