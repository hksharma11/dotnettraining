﻿using System;

namespace Harsh_11Aug_Exception
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int a = 10;
                int b = a - 10;
                int c = a / b;
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }

            try
            {
                int[] arr = new int[5];
                arr[7] = 2;
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
