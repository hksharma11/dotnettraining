﻿using System;

namespace Harsh_10_Aug_Program2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your percentage");
            int percentage = Convert.ToInt32(Console.ReadLine());
            if (percentage > 90)
                Console.WriteLine("Grade A");
            else if (percentage > 80 && percentage <= 90)
                Console.WriteLine("Grade B");
            else if (percentage > 70 && percentage <= 80)
                Console.WriteLine("Grade C");
            else if (percentage > 60 && percentage <= 70)
                Console.WriteLine("Grade D");
            else
                Console.WriteLine("Grade E");
        }
    }
}
